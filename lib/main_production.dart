import 'package:flutter/material.dart';
import 'package:terraform/config/flavor_config.dart';

import 'app.dart';

void main() {
  FlavorConfig(
    flavor: Flavor.PRODUCTION,
    variables: FlavorVariables(
      endPointUrl: "localhost:1010",
    ),
  );

  runApp(MyApp());
}
