import 'package:flutter/material.dart';

enum Flavor {
  DEV,
  STAGING,
  PRODUCTION,
}

class FlavorVariables {
  final String endPointUrl;

  FlavorVariables({@required this.endPointUrl});
}

class FlavorConfig {
  final Flavor flavor;
  final String name;
  final FlavorVariables variables;
  static FlavorConfig _instance;

  factory FlavorConfig({
    @required Flavor flavor,
    @required FlavorVariables variables,
  }) {
    _instance ??= FlavorConfig._internal(
      flavor,
      flavor.toString().split(".")[1],
      variables,
    );
    return _instance;
  }

  FlavorConfig._internal(this.flavor, this.name, this.variables);

  static FlavorConfig get instance {
    return _instance;
  }

  static bool isDev() => _instance.flavor == Flavor.DEV;
  static bool isStaging() => _instance.flavor == Flavor.STAGING;
  static bool isProduction() => _instance.flavor == Flavor.PRODUCTION;
}
