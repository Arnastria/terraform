import 'package:flutter/material.dart';
import 'package:terraform/widget/flavor_banner.dart';

import 'config/flavor_config.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return FlavorBanner(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Terraform"),
        ),
        backgroundColor: Colors.grey.shade200,
        body: Center(
          child: Text("${FlavorConfig.instance.name}"),
        ),
      ),
    );
  }
}
