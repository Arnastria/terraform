# terraform

Flutter project to display flavoring using flutter target main flag

-----------

## Flavoring

Taken from a nice medium article here.

- [Flutter Ready to Go](https://flutter.dev/docs/get-started/codelab)

Currently this project has 3 different 'flavor' :
- DEV
- STAGING
- PRODUCTION

-----------

## Running and Building

To run specific flavor you must use targeting flag in command. For example if you want to run in **STAGING** flavor use the following  command :

```flutter run -t lib/main_staging.dart```

This repo provide launch setting for visual studio code located in : ```.vscode/launch.json```

Its the same for build APK. For example if you want to build an APK release version of **PRODUCTION** flavor use the following command :

```flutter build apk --release -t lib/main_production.dart```